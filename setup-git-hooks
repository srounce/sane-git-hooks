#!/usr/bin/env node

const path = require('path')
const fs = require('fs')
const child_process = require('child_process')
const exec = child_process.execSync
const escapeRE = require('escape-regexp')

const validGitHooks = [
  'applypatch-msg',
  'pre-applypatch',
  'post-applypatch',
  'pre-commit',
  'prepare-commit-msg',
  'commit-msg',
  'post-commit',
  'pre-rebase',
  'post-checkout',
  'post-merge',
  'pre-push',
  'pre-receive',
  'update',
  'post-receive',
  'post-update',
  'push-to-checkout',
  'pre-auto-gc',
  'post-rewrite',
  'sendemail-validate'
]
const arrowIcon = '\u2192'
const checkIcon = '\u2713'
const crossIcon = '\u2717'
let gitHookRoot

const writeLn = (msg, stderr = true) =>
  (stderr ? process.stderr : process.stdout).write(`${msg}\n`)

const failureNoGitHooks = gitHookRoot => {
  writeLn(
    `Cannot install git-hooks:\n  Git hooks dir: '${gitHookRoot}' does not exist`,
    true
  )
  process.exit(1)
}

const failureNoUserHooks = hookRoot => {
  writeLn(
    `Cannot install git-hooks:\n  Directory '${hookRoot}' does not exist`,
    true
  )
  process.exit(2)
}

try {
  gitHookRoot = path.resolve(
    String(exec('git rev-parse --git-path hooks')).trim()
  )

  const gitHookRootStats = fs.statSync(gitHookRoot)
  gitHookRootStats.isDirectory() || failureNoGitHooks(gitHookRoot)
} catch (e) {
  failureNoGitHooks(gitHookRoot)
}

const userHookRoot = path.resolve(
  process.argv[2] || path.join(__dirname, 'git-hooks')
)

try {
  const userHookRootStats = fs.statSync(userHookRoot)
  userHookRootStats.isDirectory() || failureNoUserHooks(userHookRoot)
} catch (e) {
  failureNoUserHooks(userHookRoot)
}

writeLn(`Installing git-hooks:\n`)
const userHooks = fs.readdirSync(userHookRoot)
userHooks.filter(hook => ~validGitHooks.indexOf(hook)).forEach(hook => {
  const gitHookPath = path.join(gitHookRoot, hook)
  const userHookPath = path
    .relative(
      path.resolve(path.join(gitHookPath, '../..')),
      path.join(userHookRoot, hook)
    )
    .replace(/^\.\.\//, '')

  const additionalContent = `exec ${userHookPath}`
  let gitHookContent

  writeLn(`  ${arrowIcon} Installing '${hook}'\n`)
  try {
    gitHookContent = String(fs.readFileSync(gitHookPath)).trim()

    const contentRE = new RegExp(`^${escapeRE(additionalContent)}$`, 'm')
    const contentMatch = gitHookContent.match(contentRE)
    if (!contentMatch) {
      gitHookContent = `${gitHookContent}\n\n${additionalContent}`
    } else {
      writeLn(
        `    ${crossIcon} Git-hook '${hook}' already exists!\n`,
        true
      )
      return
    }
  } catch (e) {
    gitHookContent = `#!/bin/sh\n\n${additionalContent}`
  }

  writeLn(`    ${checkIcon} Installed '${hook}'\n`)
  fs.writeFileSync(gitHookPath, gitHookContent)
  fs.chmodSync(gitHookPath, 0755)
})
